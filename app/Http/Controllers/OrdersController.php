<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
    	$orders = Order::paginate();
    	return view('order.index',compact('orders'));
    }

    public function summary()
    {
        sleep(2);
    	return [
    		['display'=>'Paid' , 'value'=> '12'],
    		['display'=>'Package Gen' , 'value'=> '133'],
    		['display'=>'Shipped' , 'value'=> '1'],
    		['display'=>'Pending' , 'value'=> '9'],
    	];
    }
    public function summary2()
    {
        sleep(2);
    	return [
    		['display'=>'Jtest' , 'value'=> '12'],
    		['display'=>'hihi hello' , 'value'=> '133'],
    		['display'=>'123 123' , 'value'=> '1'],
    		['display'=>'abc' , 'value'=> '9'],
    	];
    }

    public function create()
    {
        return view('order.create');
    }
}
