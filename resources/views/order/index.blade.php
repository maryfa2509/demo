@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <ht-summary end-point="/orders/summary">Summary1asdf</ht-summary>
            <ht-summary end-point="/orders/summary2">Jtest</ht-summary>
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Orders</div>
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($orders as $order)
                            <li class="list-group-item">
                                {{$order->id}} :{{$order->title}}
                            </li>
                        @endforeach
                    </ul>                
                </div>
            </div>

            {{$orders->links()}}
        </div>
    </div>
</div>
@endsection
