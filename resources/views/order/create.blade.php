@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Form valiation</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}" @submit.prevent="validateBeforeSubmit">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" :class="{'has-error': errors.has('email') }">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input v-validate="'required|email'" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <span v-show="errors.has('email')" class="help is-danger">@{{ errors.first('email') }}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" :class="{'has-error': errors.has('jtest') }">
                            <label for="jtest" class="col-md-4 control-label">MIN 5 CHARS</label>

                            <div class="col-md-6">
                                <input v-validate="'min:5'" id="jtest" type="text" class="form-control" name="jtest" value="{{ old('jtest') }}" required >

                                @if ($errors->has('jtest'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jtest') }}</strong>
                                    </span>
                                @endif
                                <span v-show="errors.has('jtest')" class="help is-danger">{{_('please ....')}}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" :class="{'has-error': errors.has('jtest2') }">
                            <label for="jtest2" class="col-md-4 control-label">In (jtest,2,3)</label>

                            <div class="col-md-6">
                                <input v-validate="'in:jtest,2,3'" id="jtest2" type="text" class="form-control" name="jtest2" value="{{ old('jtest2') }}" required >

                                @if ($errors->has('jtest2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jtest2') }}</strong>
                                    </span>
                                @endif
                                <span v-show="errors.has('jtest2')" class="help is-danger">@{{ errors.first('jtest2') }}</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
