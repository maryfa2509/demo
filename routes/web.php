<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/orders','OrdersController@index');
Route::get('/orders/summary','OrdersController@summary');
Route::get('/orders/summary2','OrdersController@summary2');

Route::get('/orders/create','OrdersController@create');


Auth::routes();

Route::get('/home', 'HomeController@index');
